#ifndef scintCalo_h
#define scintCalo_h 1


// Geant4 Libraires
//
#include "G4Material.hh"
#include "G4LogicalVolume.hh"
#include "G4PVPlacement.hh"
#include "G4Box.hh"
#include "G4Tubs.hh"
#include "G4SystemOfUnits.hh" 
#include "G4ReflectionFactory.hh"
#include "G4SubtractionSolid.hh"


// Local Libraries
//


class scintCalo
{
  public:
    scintCalo();
    virtual ~scintCalo();

    void buildDetector(G4LogicalVolume* log_mother, std::string barName, int copy, std::string nameHole, G4double fBarHigth, G4double fBarLength, G4double fBarWidth, G4double holeRadius, G4double holeLength, G4Material* barPolystyrene, G4Material* hole_material, G4bool* overLaps);
             
    
    G4VPhysicalVolume* getPhysVolume();
    G4LogicalVolume* getLogVolume();

    G4VPhysicalVolume* getHolePhysVolume();
    G4LogicalVolume* getHoleLogVolume();

    G4double getHoleLength();

  private:

    G4Box* scint_geo;
    G4LogicalVolume* scint_log;
    G4VPhysicalVolume* scint_phys;

    G4Tubs* hole_geo;
    G4LogicalVolume* hole_log;
    G4VPhysicalVolume* hole_phys;
};
#endif
