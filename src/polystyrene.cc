// Geant4 Libraries
//
#include "G4Material.hh"
#include "G4Element.hh"
#include "G4SystemOfUnits.hh"


// C++ Libraries
//


// Local Libraries
//
#include "polystyrene.hh"


polystyrene::polystyrene()
{
  G4cout << "...Creating Polystyrene..." << G4endl;

  C = NULL;
  H = NULL;
  thePolyste = NULL;
  
  z = 0.;
  a = 0.;
  density = 0.;
}


polystyrene::~polystyrene()
{}


G4Material* polystyrene::doPolyste()
{
  C = new G4Element("Carbon", "C", z = 6 , a = 12.01*g/mole);
  H = new G4Element("Hydrogen", "H", z = 1 , a = 1.01*g/mole);

  thePolyste = new G4Material("thePolyste", density= 1.050*g/cm3, 2);
  thePolyste->AddElement(C, 8);
  thePolyste->AddElement(H, 8);

  return thePolyste;
}


G4MaterialPropertiesTable* polystyrene::doOpPolyste()
{
  G4double photonEnergy[] =
  {1.00*eV,2.03*eV,2.06*eV,2.09*eV,2.12*eV,
   2.15*eV,2.18*eV,2.21*eV,2.24*eV,2.27*eV,
   2.30*eV,2.33*eV,2.36*eV,2.39*eV,2.42*eV,
   2.45*eV,2.48*eV,2.51*eV,2.54*eV,2.57*eV,
   2.60*eV,2.63*eV,2.66*eV,2.69*eV,2.72*eV,
   2.75*eV,2.78*eV,2.81*eV,2.84*eV,2.87*eV,
   2.90*eV,2.93*eV,2.96*eV,2.99*eV,3.02*eV,
   3.05*eV,3.08*eV,3.11*eV,3.14*eV,3.17*eV,
   3.20*eV,3.23*eV,3.26*eV,3.29*eV,3.32*eV,
   3.35*eV,3.38*eV,3.41*eV,3.44*eV,5.47*eV};

  const G4int nEntries = sizeof(photonEnergy)/sizeof(G4double);

  G4double refractiveIndexPS[] =
  { 1.50, 1.50, 1.50, 1.50, 1.50, 1.50, 1.50, 1.50, 1.50, 1.50,
    1.50, 1.50, 1.50, 1.50, 1.50, 1.50, 1.50, 1.50, 1.50, 1.50,
    1.50, 1.50, 1.50, 1.50, 1.50, 1.50, 1.50, 1.50, 1.50, 1.50,
    1.50, 1.50, 1.50, 1.50, 1.50, 1.50, 1.50, 1.50, 1.50, 1.50,
    1.50, 1.50, 1.50, 1.50, 1.50, 1.50, 1.50, 1.50, 1.50, 1.50};

  assert(sizeof(refractiveIndexPS) == sizeof(photonEnergy));

  G4double absPS[] =
  {2.*cm,2.*cm,2.*cm,2.*cm,2.*cm,2.*cm,2.*cm,2.*cm,2.*cm,2.*cm,
   2.*cm,2.*cm,2.*cm,2.*cm,2.*cm,2.*cm,2.*cm,2.*cm,2.*cm,2.*cm,
   2.*cm,2.*cm,2.*cm,2.*cm,2.*cm,2.*cm,2.*cm,2.*cm,2.*cm,2.*cm,
   2.*cm,2.*cm,2.*cm,2.*cm,2.*cm,2.*cm,2.*cm,2.*cm,2.*cm,2.*cm,
   2.*cm,2.*cm,2.*cm,2.*cm,2.*cm,2.*cm,2.*cm,2.*cm,2.*cm,2.*cm};

  assert(sizeof(absPS) == sizeof(photonEnergy));

  G4double scintilFast[] =
  {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
   0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
   0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
   1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0,
   1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0};

  assert(sizeof(scintilFast) == sizeof(photonEnergy));

  // Add entries into properties table
  G4MaterialPropertiesTable* mptPolystyrene = new G4MaterialPropertiesTable();

  mptPolystyrene->AddProperty("RINDEX",photonEnergy,refractiveIndexPS,nEntries);
  mptPolystyrene->AddProperty("ABSLENGTH",photonEnergy,absPS,nEntries);
  mptPolystyrene->AddProperty("FASTCOMPONENT",photonEnergy, scintilFast,nEntries);
  mptPolystyrene->AddConstProperty("SCINTILLATIONYIELD",10./keV);
  mptPolystyrene->AddConstProperty("RESOLUTIONSCALE",1.0);
  mptPolystyrene->AddConstProperty("FASTTIMECONSTANT", 10.*ns);
 
  return mptPolystyrene;
}
