// Geant4 Libraries
//
#include "Randomize.hh"
#include "G4Event.hh"
#include "G4ParticleGun.hh"
#include "G4ParticleTable.hh"
#include "G4ParticleDefinition.hh"
#include "G4SystemOfUnits.hh"


// Local Libraries
//
#include "PrimaryGeneratorAction.hh"
#include "PrimaryGeneratorMessenger.hh"
#include "PrimarySpectrum.hh"


namespace
{
  G4Mutex aMutex = G4MUTEX_INITIALIZER;
}


PrimaryGeneratorAction::PrimaryGeneratorAction(PrimarySpectrum* crkPar)
  : G4VUserPrimaryGeneratorAction(),
  fCrkPar(crkPar),
  fParticleGun(0)
{
  G4cout << "...PrimaryGeneratorAction..." << G4endl;

  G4int n_particle = 1;
  fParticleGun = new G4ParticleGun(n_particle);

  //create a messenger for this class
  fGunMessenger = new PrimaryGeneratorMessenger(this);

  //particleTable = G4ParticleTable::GetParticleTable();


  //default kinematic
  //
  
  primaryId = "mu-";
  particle = particleTable->FindParticle(primaryId);

  fParticleGun->SetParticleDefinition(particle);
  
//  fParticleGun->SetParticleTime(0.0*ns);
//  fParticleGun
//    ->SetParticlePosition(G4ThreeVector(30.*cm, 30.*cm, 70.*cm));//.0*cm, 61.0*cm, 0.*cm));//61.0*cm));
//  fParticleGun
//    ->SetParticleMomentumDirection(G4ThreeVector(-1., 0., 0.));
}


PrimaryGeneratorAction::~PrimaryGeneratorAction()
{
  delete fParticleGun;
  delete fGunMessenger;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void PrimaryGeneratorAction::GeneratePrimaries(G4Event* anEvent)
{
  G4AutoLock l(&aMutex);
/*
  fCrkPar->primaryPosition();
  fCrkPar->primaryMomento();   

  position = fCrkPar->particlePosition;
  direction = fCrkPar->particleDirection;
  primaryId = fCrkPar->parId;

  particle = particleTable->FindParticle(primaryId);

  fParticleGun->SetParticlePosition(position);
  fParticleGun->SetParticleMomentum(direction);
  fParticleGun->SetParticleDefinition(particle); 

  fParticleGun->GeneratePrimaryVertex(anEvent);

  l.unlock();
  */
  fParticleGun->GeneratePrimaryVertex(anEvent);
  //G4cout << "on primaryGenerator" << G4endl;   
}


void PrimaryGeneratorAction::SetOptPhotonPolar()
{
 G4double angle = G4UniformRand() * 360.0*deg;
 SetOptPhotonPolar(angle);
}


void PrimaryGeneratorAction::SetOptPhotonPolar(G4double angle)
{
 if (fParticleGun->GetParticleDefinition()->GetParticleName()!="opticalphoton")
   {
     G4cout << "--> warning from PrimaryGeneratorAction::SetOptPhotonPolar() :"
               "the particleGun is not an opticalphoton" << G4endl;
     return;
   }

 G4ThreeVector normal (1., 0., 0.);
 G4ThreeVector kphoton = fParticleGun->GetParticleMomentumDirection();
 G4ThreeVector product = normal.cross(kphoton);
 G4double modul2       = product*product;
 
 G4ThreeVector e_perpend (0., 0., 1.);
 if (modul2 > 0.) e_perpend = (1./std::sqrt(modul2))*product;
 G4ThreeVector e_paralle    = e_perpend.cross(kphoton);
 
 G4ThreeVector polar = std::cos(angle)*e_paralle + std::sin(angle)*e_perpend;
 fParticleGun->SetParticlePolarization(polar);
}

