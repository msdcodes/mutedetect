// Geant4 Libraries
//
#include "G4VUserDetectorConstruction.hh"
#include "G4PVPlacement.hh"
#include "G4LogicalVolume.hh"
#include "G4ThreeVector.hh"
#include "G4Material.hh"
#include "G4Element.hh"
#include "G4LogicalVolume.hh"
#include "G4Box.hh"
#include "G4UnitsTable.hh"
#include "G4SystemOfUnits.hh" 
#include "G4OpticalSurface.hh"
#include "G4LogicalBorderSurface.hh"
#include "G4NistManager.hh"
#include "G4LogicalSkinSurface.hh"


// Local Libraries
//
#include "scintCont.hh"


scintCont::scintCont()
{
  G4cout << "...sinctCont..." << G4endl;
}


scintCont::~scintCont()
{}


void scintCont::buildDetector(G4LogicalVolume* log_mother, G4RotationMatrix* rot, G4ThreeVector pos, std::string nameClone, int copy, G4String barName, G4double fBarHigth, G4double fBarLength, G4double fBarWidth, G4Material* coating_tio2, G4OpticalSurface* coating_opSurf, G4bool* overLaps)
{
  coating_geo = new G4Box(
      barName,
      fBarWidth, // x
      fBarHigth, // y
      fBarLength // z
      );

  coating_log = new G4LogicalVolume(
      coating_geo,
      coating_tio2,
      coating_geo->GetName()
      );

  new G4LogicalSkinSurface("TiO2Surface", coating_log, coating_opSurf);


  coating_phys = new G4PVPlacement(
      rot,
      pos, 
      coating_log,
      nameClone,
      log_mother,
      false,
      copy,
      overLaps
      );
}


G4VPhysicalVolume* scintCont::getPhysVolume()
{
  return coating_phys;
}


G4LogicalVolume* scintCont::getLogVolume()
{
  return coating_log;
}
