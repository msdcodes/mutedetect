// Geant4 Libraries
//


// Local Libraries
//
#include "ActionInitialization.hh"
#include "PrimaryGeneratorAction.hh"
#include "RunAction.hh"
#include "EventAction.hh"
#include "TrackingAction.hh"
#include "SteppingAction.hh"
#include "SteppingVerbose.hh"
#include "PrimarySpectrum.hh"


ActionInitialization::ActionInitialization()
 : G4VUserActionInitialization()
{
   G4cout << "...ActionInitialization..." << G4endl;
   parCrk = new PrimarySpectrum(); 
}


ActionInitialization::~ActionInitialization()
{}


void ActionInitialization::BuildForMaster() const
{
  RunAction* runAction = new RunAction;
  SetUserAction(runAction);
}


void ActionInitialization::Build() const
{

  RunAction* runAction = new RunAction();
  SetUserAction(runAction);

  PrimaryGeneratorAction* primaryGenAction = new PrimaryGeneratorAction(parCrk);
  SetUserAction(primaryGenAction);

  EventAction* eventAction = new EventAction(runAction, primaryGenAction);
  SetUserAction(eventAction);

  TrackingAction* trackAction = new TrackingAction(eventAction);
  SetUserAction(trackAction);

  SteppingAction* stepAction = new SteppingAction(eventAction, trackAction);
  SetUserAction(stepAction);
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4VSteppingVerbose* ActionInitialization::InitializeSteppingVerbose() const
{
  return new SteppingVerbose();
}  

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
