// Geant4 Libraries
//
#include "G4Track.hh"
#include "G4OpticalPhoton.hh"


// Local Libraries
//
#include "TrackingAction.hh"
#include "EventAction.hh"


G4ThreadLocal int TrackingAction::primary;
G4ThreadLocal int TrackingAction::optical;
G4ThreadLocal int TrackingAction::secondary;


TrackingAction::TrackingAction(EventAction* eventAction)
  : fEventAction(eventAction)
{}


TrackingAction::~TrackingAction()
{}


void TrackingAction::PreUserTrackingAction(const G4Track* track)
{
  if ( track->GetDefinition() == G4OpticalPhoton::OpticalPhotonDefinition() )
    optical = 1;
  else
    optical = 0;


  if ( track->GetParentID() == 0 )
  {
    primary = 1;
    if ( track->GetCurrentStepNumber() == 0 )
    {
      fEventAction->prmName = track->GetParticleDefinition()->GetParticleName();
      (*fEventAction->prmMomentum) = track->GetMomentum();
      (*fEventAction->prmStartPos) = track->GetPosition();
    }
  }
  else
    primary = 0;

  if ( track->GetParentID() > 0 )
    secondary = 1;
  else
    secondary = 0;

/*
  G4cout << "PreUser: "
    << track->GetGlobalTime() << " "
    << G4endl;
    */
}

/*
void TrackingAction::PostUserTrackingAction(const G4Track* track)
{
  
  G4cout << "PostUser: "
    << track->GetGlobalTime() << " "
    << G4endl << G4endl;
    
}
*/
