// Geant4 Libraries
//
#include "G4Material.hh"
#include "G4PhysicalConstants.hh"
#include "G4OpticalSurface.hh"
#include "G4LogicalBorderSurface.hh"


// Local Libraries
//
#include "fiberClad2.hh"
#include "pethyleneF.hh"


fiberClad2::fiberClad2()
{}


fiberClad2::~fiberClad2()
{}


void fiberClad2::buildDetector(G4LogicalVolume* log_mother, std::string cld2Name, int copy, G4double fWLSfiberZ, G4double fClad1RX, G4double fWLSfiberRX, G4Material* cld2Mat, G4bool* overLaps)
{

  G4double fClad2Z = fWLSfiberZ;
  G4double fClad2RX = fClad1RX + 0.03*fWLSfiberRX;

  fiberClad2_geo =
    new G4Tubs (
        cld2Name,
        fClad1RX,
        fClad2RX,
        fClad2Z,
        0.0*rad,
        twopi*rad
        );

  fiberClad2_log =
    new G4LogicalVolume(
        fiberClad2_geo,
        cld2Mat,
        cld2Name,
        0,
        0,
        0
        );

  fiberClad2_phys =
    new G4PVPlacement(
        0,
        G4ThreeVector(),
        fiberClad2_log,
        cld2Name,
        log_mother,
        false,
        copy,
        overLaps
        );
}


G4LogicalVolume* fiberClad2::getLogVolume()
{
  return fiberClad2_log;
}


G4VPhysicalVolume* fiberClad2::getPhysVolume()
{
  return fiberClad2_phys;
}
