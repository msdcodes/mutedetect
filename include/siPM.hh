#ifndef siPM_h
#define siPM_h 1


// Geant4 Libraries
//
#include "G4Material.hh"
#include "G4LogicalVolume.hh"
#include "G4PVPlacement.hh"
#include "G4Box.hh"

// Local Libraries
//


class siPM 
{
   public:
     siPM();
    virtual ~siPM();

    void buildDetector(G4LogicalVolume* log_mother, G4ThreeVector pos, std::string name, int copy, G4Material* sipm_mat, G4bool* overLaps);

    G4VPhysicalVolume* getPhysVolume();

  private:

    G4Box* sipm_geo;
    G4LogicalVolume* sipm_log;
    G4VPhysicalVolume* sipm_phy;
};
#endif

