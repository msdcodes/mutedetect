// Geant4 Libraries
//
#include "G4Material.hh"
#include "G4PhysicalConstants.hh"
#include "G4OpticalSurface.hh"
#include "G4LogicalBorderSurface.hh"


// Local Libraries
//
#include "fiberClad1.hh"
#include "pethyleneF.hh"


// C++ Libraries
//
#include "string.h"


fiberClad1::fiberClad1(G4double fWLSfiberRX)
{
  fClad1RX = fWLSfiberRX + 0.03*fWLSfiberRX;
}


fiberClad1::~fiberClad1()
{}


void fiberClad1::buildDetector(G4LogicalVolume* log_mother, std::string cld1Name, int copy, G4double fWLSfiberZ, G4double fWLSfiberRX, G4Material* cldMat, G4bool* overLaps)
{
  G4double fClad1Z = fWLSfiberZ;

  fiberClad1_geo =
    new G4Tubs (
        cld1Name,
        fWLSfiberRX,
        fClad1RX,
        fClad1Z,
        0.0*rad,
        twopi*rad
        );

  fiberClad1_log =
    new G4LogicalVolume(
        fiberClad1_geo,
        cldMat,
        cld1Name,
        0,
        0,
        0
        );

 fiberClad1_phys =
   new G4PVPlacement(
       0,
       G4ThreeVector(),
       fiberClad1_log,
       cld1Name,
       log_mother,
       false,
       copy,
       overLaps
       );
}


G4LogicalVolume* fiberClad1::getLogVolume()
{
  return fiberClad1_log;
}


G4VPhysicalVolume* fiberClad1::getPhysVolume()
{
  return fiberClad1_phys;
}


G4double fiberClad1::getRX()
{
  return fClad1RX;
}

