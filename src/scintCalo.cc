// Geant4 Libraries
//
#include "G4Material.hh"
#include "G4NistManager.hh"
#include "G4ReflectionFactory.hh"
#include "G4SubtractionSolid.hh"


// C++ Libraries
//


// Local Libraries
//
#include "scintCalo.hh"
#include "polystyrene.hh"
#include "pTerphenylPopop.hh"


scintCalo::scintCalo()
{}


scintCalo::~scintCalo()
{}


void scintCalo::buildDetector(G4LogicalVolume* log_mother, std::string barName, int copy, std::string nameHole, G4double fBarHigth, G4double fBarLength, G4double fBarWidth, G4double holeRadius, G4double holeLength, G4Material* barPolystyrene, G4Material* hole_material, G4bool* overLaps)
{
   scint_geo = 
    new G4Box(
        barName,
        fBarWidth - 0.5*mm, // x
        fBarHigth - 0.5*mm, // y
        fBarLength - 0.5*mm // z
        );

  scint_log =
    new G4LogicalVolume(
        scint_geo,
        barPolystyrene,
        scint_geo->GetName(),
        0,
        0,
        0
        );

  hole_geo =
    new G4Tubs(
        "hole_geo",
        0., 
        holeRadius,
        holeLength - 0.5*mm,
        0.*deg,
        360.*deg
        );

  hole_log =
    new G4LogicalVolume(
        hole_geo,
        hole_material,
        hole_geo->GetName(),
        0,
        0,
        0
        );

  scint_phys =
    new G4PVPlacement(
        0,
        G4ThreeVector(),
        scint_log,
        barName,
        log_mother,
        false,
        copy,
        overLaps
        );

  hole_phys =
    new G4PVPlacement(
        0,
        G4ThreeVector(),
        hole_log,
        nameHole,
        scint_log,
        false,
        copy,
        overLaps
        );
}


G4VPhysicalVolume* scintCalo::getPhysVolume()
{
  return scint_phys;
}


G4LogicalVolume* scintCalo::getLogVolume()
{
  return scint_log;
}


G4LogicalVolume* scintCalo::getHoleLogVolume()
{
  return hole_log;
}


G4VPhysicalVolume* scintCalo::getHolePhysVolume()
{
  return hole_phys;
}
