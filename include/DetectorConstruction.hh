#ifndef DetectorConstruction_h
#define DetectorConstruction_h 1


// Geant4 Libraries
//
#include "globals.hh"
#include "G4VUserDetectorConstruction.hh"
#include "G4Cache.hh"
#include "G4Material.hh"
#include "G4Element.hh"
#include "G4LogicalVolume.hh"
#include "G4Box.hh"
#include "G4Ellipsoid.hh"
#include "G4Sphere.hh"
#include "G4Tubs.hh"
#include "G4OpticalSurface.hh"


// Local Libraries
//
#include "world.hh"
#include "expHall.hh"
#include "volcano.hh"
#include "grdFloor.hh"
#include "muteBox.hh"
#include "wcdLagoCont.hh"
#include "wcdLagoCalo.hh"
#include "wcdpmt.hh"
#include "scintCont.hh"
#include "scintCalo.hh"
#include "wlsFiber.hh"
#include "siPM.hh"


class G4LogicalVolume; 


class DetectorConstruction : public G4VUserDetectorConstruction
{
  public:
    DetectorConstruction();
    virtual ~DetectorConstruction();
    virtual G4VPhysicalVolume* Construct();


  private:

    // -----------------
    // *** The World ***
    // -----------------
    world* universe;
    expHall* theExpHall;


    // -----------------
    // *** Buildings ***
    // -----------------
    grdFloor* grd;
    volcano* volc;

    muteBox* mtBox;

    wcdLagoCont* wcdCont;
    wcdLagoCalo* wcdCalo;
    wcdpmt* pmt_det;

    scintCont* sctCont0Y;
    scintCont* sctCont0X;
    scintCont* sctCont1Y;
    scintCont* sctCont1X;

    scintCalo* sctCalo0Y;
    scintCalo* sctCalo0X;
    scintCalo* sctCalo1Y;
    scintCalo* sctCalo1X;

    wlsFiber* fiber0Y;
    wlsFiber* fiber0X;
    wlsFiber* fiber1Y;
    wlsFiber* fiber1X;

    siPM* sipm0Y;
    siPM* sipm0X;
    siPM* sipm1Y;
    siPM* sipm1X;
};
#endif
