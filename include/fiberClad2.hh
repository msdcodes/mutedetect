#ifndef fiberClad2_h
#define fiberClad2_h 1


// Geant4 Libraries
//
#include "G4Material.hh"
#include "G4LogicalVolume.hh"
#include "G4PVPlacement.hh"
#include "G4Tubs.hh"
#include "G4SystemOfUnits.hh"
#include "G4ReflectionFactory.hh"


// C++ Libraries
//
#include "string.h"


class fiberClad2
{
  public:
    fiberClad2();
    virtual ~fiberClad2();

    void buildDetector(G4LogicalVolume* log_mother, std::string cld2Name, int copy, G4double fWLSfiberZ, G4double fClad1RX, G4double fWLSfiberRX, G4Material* cld2Mat, G4bool* overLaps);
        
    G4VPhysicalVolume* getPhysVolume();
    G4LogicalVolume* getLogVolume();

  private:

     G4Tubs* fiberClad2_geo;
     G4LogicalVolume* fiberClad2_log;
     G4VPhysicalVolume* fiberClad2_phys;
};
#endif



