#ifndef volcano_h
#define volcano_h 1


// Geant4 Libraries
//
#include "G4Element.hh"
#include "G4LogicalVolume.hh"
#include "G4PVPlacement.hh"
#include "G4Ellipsoid.hh"
#include "G4SystemOfUnits.hh" 


// Local Libraries
//


class volcano
{
  public:
    volcano();
    ~volcano();

    void DefineMaterials();
    void buildDetector(G4LogicalVolume* log_mother, G4double pxSemiAxis, G4double pySemiAxis, G4double pzSemiAxis, G4ThreeVector* volcPost, G4bool* overLaps);

    G4VPhysicalVolume* getPhysVolume();
    G4LogicalVolume* getLogVolume();


  private:
    G4Material* volcano_mat;

    G4Ellipsoid* volcano_geo;
    G4LogicalVolume* volcano_log;
    G4VPhysicalVolume* volcano_phys;
};
#endif
