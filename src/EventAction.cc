// Geant4 Libraries
//
#include "G4UserEventAction.hh"
#include "G4Event.hh"
#include "G4Track.hh"
#include "g4root.hh"
#include "G4SystemOfUnits.hh"


// C++ Libraries
//
#include <cmath>


// Local Libraries
//
#include "EventAction.hh"
#include "RunAction.hh"
#include "PrimaryGeneratorAction.hh"


G4ThreadLocal int EventAction::sipmThr0 = 0;
G4ThreadLocal int EventAction::sipmThr1 = 0;
G4ThreadLocal int EventAction::pmtThr = 0;
G4ThreadLocal G4double EventAction::trcRock = 0.;
G4ThreadLocal int EventAction::prmTime = 0;
G4ThreadLocal int EventAction::timeSp0 = 0;
G4ThreadLocal int EventAction::timeSp1 = 0;


G4ThreadLocal std::vector < std::vector < int > >* EventAction::trcSipm;
G4ThreadLocal std::vector < int >* EventAction::trcPmt;

G4ThreadLocal G4ThreeVector* EventAction::prmMomentum;
G4ThreadLocal G4ThreeVector* EventAction::prmStartPos;
G4ThreadLocal G4ThreeVector* EventAction::prmOutVolcPoi;
G4ThreadLocal G4ThreeVector* EventAction::prmOutVolcMom;

G4ThreadLocal G4String EventAction::prmName;
G4ThreadLocal int EventAction::traceLenght;


EventAction::EventAction(RunAction* runAction, PrimaryGeneratorAction* primaryGenAction)
  : G4UserEventAction(),
  fRunAction(runAction),
  fPriGenAction(primaryGenAction)
{
  G4cout << "...EventAction..." << G4endl;

  trcSipm = new std::vector < std::vector < int > >;
  trcPmt = new std::vector < int >;

  traceLenght = 300; // Time Resolution: 30 for 10ns, 300 for 1ns

  trcSipm->resize( traceLenght ); // traceLenght bins of 10 ns each one
  trcPmt->resize( traceLenght/10 );

  for ( int t=0; t < traceLenght; t++ )
    for ( int i=0; i < 120; i++ )
      (*trcSipm)[t].push_back( 0 );
}


EventAction::~EventAction()
{}


void EventAction::BeginOfEventAction(const G4Event* event)
{
 /* 
  G4cout << "Begin of Event: "
    << event->GetEventID()
    << G4endl;
    */    
    
  for ( int t=0; t < traceLenght; t++ ) // binwidth 10 ns
  {
    if ( t < 30 )
      (*trcPmt)[t] = 0;

    for ( int i=0; i < 120; i++ ) // 120 sipm code name
      (*trcSipm)[t][i] = 0;
  }

  prmTime = 1;
  trcRock = 0.;
 
  sipmThr0 = 0;
  sipmThr1 = 0;
  pmtThr = 0;
  timeSp0 = 0;
  timeSp1 = 0;

  prmMomentum = new G4ThreeVector();
  prmStartPos = new G4ThreeVector();
  prmOutVolcPoi = new G4ThreeVector();
  prmOutVolcMom = new G4ThreeVector();
}


void EventAction::EndOfEventAction(const G4Event* event)
{
/*  
  G4cout << "End of Event: "
    << event->GetEventID()
    << G4endl;
  */  

  int binThd = 0;
  while ( !sipmThr0 && binThd < 300 )
  {
    for ( int i=0; i < 60; i++ ) // moving on sipm
      if ( (*trcSipm)[binThd][i] > 3 )
      {
        sipmThr0 = 1;
        timeSp0 = binThd;
        break;
      }
    binThd++;
  }

  binThd = 0;
  while ( !sipmThr1 && binThd < 300 )
  {
    for ( int i = 60; i < 120; i++ )
      if ( (*trcSipm)[binThd][i] > 3 )
      {
        sipmThr1 = 1;
        timeSp1 = binThd;
        break;
      }
    binThd++;
  }


  if ( sipmThr0 || sipmThr1 ) 
  {
    for ( int t=0; t < traceLenght; t++ )
    {
      //G4cout << "sp ";
      (*fRunAction->outFile) << "sp ";

      for ( int i=0; i < 120; i++ ) // moving on sipm
      {
        //G4cout << " " << (*trcSipm)[t][i];
        (*fRunAction->outFile) << " " << (*trcSipm)[t][i];
        (*trcSipm)[t][i] = 0; 
      }

      //G4cout << G4endl;
      (*fRunAction->outFile) << G4endl;
    }
  }

  traceLenght = 30; // change for WCD resolution
  if ( pmtThr )
    for ( int t=0; t < traceLenght; t++ )
    {
      //G4cout << "pt " << (*trcPmt)[t] << G4endl;
      (*fRunAction->outFile) << "pt " << (*trcPmt)[t] << G4endl;
      (*trcPmt)[t] = 0;
    }

  if ( sipmThr0 || sipmThr1 || pmtThr )
  {
    //G4cout << "# "
    (*fRunAction->outFile) << "# "
      << event->GetEventID()
      << " " << prmName
      << " " << trcRock / (1.*m)
      << " " << prmStartPos->getX() / (1.*m)
      << " " << prmStartPos->getY() / (1.*m)
      << " " << prmStartPos->getZ() / (1.*m)
      << " " << prmMomentum->getX() / (1.*GeV)
      << " " << prmMomentum->getY() / (1.*GeV)
      << " " << prmMomentum->getZ() / (1.*GeV)
      << " " << prmOutVolcPoi->getX() / (1.*m)
      << " " << prmOutVolcPoi->getY() / (1.*m)
      << " " << prmOutVolcPoi->getZ() / (1.*m)
      << " " << prmOutVolcMom->getX() / (1.*GeV)
      << " " << prmOutVolcMom->getY() / (1.*GeV)
      << " " << prmOutVolcMom->getZ() / (1.*GeV)
      << " " << timeSp0
      << " " << timeSp1
      << G4endl;  
  }

  trcRock = 0.;
  sipmThr0 = 0;
  sipmThr1 = 0;
  pmtThr = 0;
} 

