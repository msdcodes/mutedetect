#ifndef muteBox_h
#define muteBox_h 1


// Geant4 Libraries
//
#include "G4Material.hh"
#include "G4LogicalVolume.hh"
#include "G4PVPlacement.hh"
#include "G4Box.hh"
#include "G4SystemOfUnits.hh" 


// Local Libraries
//


class muteBox
{
  public:
    muteBox();
    virtual ~muteBox();

    void DefineMaterials();
    void buildDetector(G4double, G4double, G4double, G4LogicalVolume* log_mother, G4RotationMatrix*, G4ThreeVector pos, G4bool* overLaps);

    G4VPhysicalVolume* getPhysVolume();
    G4LogicalVolume* getLogVolume();

  private:

    G4Material* muteBox_matter;
    G4Box* muteBox_geo;
    G4LogicalVolume* muteBox_log;
    G4VPhysicalVolume* muteBox_phys;
};
#endif
