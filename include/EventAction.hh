#ifndef UserEventAction_h 
#define UserEventAction_h 1


// Geant4 Libraries
//
#include "G4UserEventAction.hh"
#include "G4Event.hh"
#include "G4Track.hh"
#include "RunAction.hh"
#include "G4ThreeVector.hh"


// Local Libraries
//
#include "PrimaryGeneratorAction.hh"


// C++ Libraries
//
#include <string>
#include <vector>


class EventAction : public G4UserEventAction
{
  public:
    EventAction(RunAction* runAction, PrimaryGeneratorAction* primaryGenAction);
    virtual ~EventAction();

    virtual void BeginOfEventAction(const G4Event *event);
    virtual void EndOfEventAction(const G4Event *event);

    RunAction* fRunAction;
    PrimaryGeneratorAction* fPriGenAction;

    static G4ThreadLocal std::vector < std::vector < int > >* trcSipm;
    static G4ThreadLocal std::vector < int >* trcPmt;
    
    static G4ThreadLocal G4double trcRock;
    static G4ThreadLocal int prmTime;
    
    static G4ThreadLocal int sipmThr0;
    static G4ThreadLocal int sipmThr1;
    static G4ThreadLocal int pmtThr;
    static G4ThreadLocal int timeSp0;
    static G4ThreadLocal int timeSp1;

   
    static G4ThreadLocal G4ThreeVector* prmMomentum;
    static G4ThreadLocal G4ThreeVector* prmStartPos;
    static G4ThreadLocal G4ThreeVector* prmOutVolcPoi;
    static G4ThreadLocal G4ThreeVector* prmOutVolcMom;

    static G4ThreadLocal G4String prmName;
    
  private:
    static G4ThreadLocal int traceLenght;
};

#endif

