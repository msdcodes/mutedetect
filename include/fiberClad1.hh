#ifndef fiberClad1_h
#define fiberClad1_h 1


// Geant4 Libraries
//
#include "G4Material.hh"
#include "G4LogicalVolume.hh"
#include "G4PVPlacement.hh"
#include "G4Tubs.hh"
#include "G4SystemOfUnits.hh"
#include "G4ReflectionFactory.hh"


class fiberClad1
{
  public:
    fiberClad1(G4double fWLSfiberRX);
    virtual ~fiberClad1();

    void buildDetector(G4LogicalVolume* log_mother, std::string cld1Name, int copy, G4double fWLSfiberZ, G4double fWLSfiberRX, G4Material* cldMat, G4bool* overLaps);

    G4double getRX();

    G4VPhysicalVolume* getPhysVolume();
    G4LogicalVolume* getLogVolume();

  private:

     G4Tubs* fiberClad1_geo;
     G4LogicalVolume* fiberClad1_log;
     G4VPhysicalVolume* fiberClad1_phys;

     G4double fClad1RX;
};
#endif
