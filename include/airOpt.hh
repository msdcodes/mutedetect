#ifndef airOpt_h
#define airOpt_h 1


// Geant4 Libraries
//
#include "G4Material.hh"
#include "G4Element.hh"
#include "G4OpticalSurface.hh"
#include "G4MaterialPropertiesTable.hh"


// Local Libraries
//


class airOpt
{
  public:
    airOpt();
    ~airOpt();
    G4Material* doAirOpt();

  private:

    G4Material* hole_material;
};
#endif

