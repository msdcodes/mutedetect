// Geant4 Libraries
//
#include "G4Material.hh"
#include "G4Element.hh"
#include "G4SystemOfUnits.hh"


// C++ Libraries
//
#include "string.h"


// Local Libraries
//
#include "pmmaFiber.hh"


pmmaFiber::pmmaFiber()
{
  G4cout << "...Creating PMMA..." << G4endl;

  C = NULL;
  H = NULL;
  thePmma = NULL;
  
  z = 0.;
  a = 0.;
  density = 0.;
}


pmmaFiber::~pmmaFiber()
{}


G4Material* pmmaFiber::doPmma()
{
  C = new G4Element("Carbon", "C", z = 6 , a = 12.01*g/mole);
  H = new G4Element("Hydrogen", "H", z = 1 , a = 1.01*g/mole);
  O = new G4Element("Oxygen"  , "O", z = 8 , a = 16.00*g/mole);

  thePmma = new G4Material("wlsScint", density= 1.190*g/cm3, 3);
  thePmma->AddElement(C, 5);
  thePmma->AddElement(H, 8);
  thePmma->AddElement(O, 2);

  return thePmma;
}


G4MaterialPropertiesTable* pmmaFiber::doOpPmma()
{

  G4double photonEnergy[] =
  { 2.00*eV,2.03*eV,2.06*eV,2.09*eV,2.12*eV,
    2.15*eV,2.18*eV,2.21*eV,2.24*eV,2.27*eV,
    2.30*eV,2.33*eV,2.36*eV,2.39*eV,2.42*eV,
    2.45*eV,2.48*eV,2.51*eV,2.54*eV,2.57*eV,
    2.60*eV,2.63*eV,2.66*eV,2.69*eV,2.72*eV,
    2.75*eV,2.78*eV,2.81*eV,2.84*eV,2.87*eV,
    2.90*eV,2.93*eV,2.96*eV,2.99*eV,3.02*eV,
    3.05*eV,3.08*eV,3.11*eV,3.14*eV,3.17*eV,
    3.20*eV,3.23*eV,3.26*eV,3.29*eV,3.32*eV,
    3.35*eV,3.38*eV,3.41*eV,3.44*eV,3.47*eV
  };

  const G4int nEntries = sizeof(photonEnergy)/sizeof(G4double);


  G4double refractiveIndexWLSfiber[] =
  { 1.60, 1.60, 1.60, 1.60, 1.60, 1.60, 1.60, 1.60, 1.60, 1.60,
    1.60, 1.60, 1.60, 1.60, 1.60, 1.60, 1.60, 1.60, 1.60, 1.60,
    1.60, 1.60, 1.60, 1.60, 1.60, 1.60, 1.60, 1.60, 1.60, 1.60,
    1.60, 1.60, 1.60, 1.60, 1.60, 1.60, 1.60, 1.60, 1.60, 1.60,
    1.60, 1.60, 1.60, 1.60, 1.60, 1.60, 1.60, 1.60, 1.60, 1.60
  };

  assert(sizeof(refractiveIndexWLSfiber) == sizeof(photonEnergy));

  G4double absWLSfiber[] =
  { 5.40*m,5.40*m,5.40*m,5.40*m,5.40*m,5.40*m,5.40*m,5.40*m,5.40*m,5.40*m,
    5.40*m,5.40*m,5.40*m,5.40*m,5.40*m,5.40*m,5.40*m,5.40*m,5.40*m,5.40*m,
    5.40*m,5.40*m,5.40*m,5.40*m,5.40*m,5.40*m,5.40*m,5.40*m,5.40*m,1.10*m,
    1.10*m,1.10*m,1.10*m,1.10*m,1.10*m,1.10*m, 1.*mm, 1.*mm, 1.*mm, 1.*mm,
    1.*mm, 1.*mm, 1.*mm, 1.*mm, 1.*mm, 1.*mm, 1.*mm, 1.*mm, 1.*mm, 1.*mm
  };

  assert(sizeof(absWLSfiber) == sizeof(photonEnergy));

  G4double emissionFib[] =
  { 0.05, 0.10, 0.30, 0.50, 0.75, 1.00, 1.50, 1.85, 2.30, 2.75,
    3.25, 3.80, 4.50, 5.20, 6.00, 7.00, 8.50, 9.50, 11.1, 12.4,
    12.9, 13.0, 12.8, 12.3, 11.1, 11.0, 12.0, 11.0, 17.0, 16.9,
    15.0, 9.00, 2.50, 1.00, 0.05, 0.00, 0.00, 0.00, 0.00, 0.00,
    0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00
  };

  assert(sizeof(emissionFib) == sizeof(photonEnergy));

  // Add entries into properties table
  G4MaterialPropertiesTable* mptWLSfiber = new G4MaterialPropertiesTable();
  mptWLSfiber->
           AddProperty("RINDEX",photonEnergy,refractiveIndexWLSfiber,nEntries);
  // mptWLSfiber->AddProperty("ABSLENGTH",photonEnergy,absWLSfiber,nEntries);
  mptWLSfiber->AddProperty("WLSABSLENGTH",photonEnergy,absWLSfiber,nEntries);
  mptWLSfiber->AddProperty("WLSCOMPONENT",photonEnergy,emissionFib,nEntries);
  mptWLSfiber->AddConstProperty("WLSTIMECONSTANT", 0.5*ns);

  return mptWLSfiber;
}


