// Geant4 Libraries
//
#include "G4Material.hh"
#include "G4Element.hh"
#include "G4SystemOfUnits.hh"
#include "G4NistManager.hh"


// C++ Libraries
//


// Local Libraries
//
#include "tio2.hh"


tio2::tio2()
{}


tio2::~tio2()
{}

G4Material* tio2::doTiO2()
{
  G4cout << "...Creating TiO2..." << G4endl;

  fExtrusionReflectivity = 1.;
  fExtrusionPolish = 1.;

  C = new G4Element("Carbon"  , "C", 6, 12.01*g/mole);
  O = new G4Element("Oxygen"  , "O", 8, 16.00*g/mole);
  H = new G4Element("Hydrogen", "H", 1, 1.01*g/mole);
  Ti = new G4Element("Titanium", "Ti", 22, 47.867*g/mole);

  TiO2 = new G4Material("TiO2", 4.26*g/cm3, 2);
  TiO2->AddElement(Ti, 1);
  TiO2->AddElement(O, 2);

  Polystyrene = new G4Material("Polystyrene", 1.050*g/cm3, 2);
  Polystyrene->AddElement(C, 8);
  Polystyrene->AddElement(H, 8);


  coating_tio2 = new G4Material("Coating", 1.52*g/cm3, 2);

  coating_tio2->AddMaterial(TiO2, 15.0*perCent);
  coating_tio2->AddMaterial(Polystyrene, 85.0*perCent);

  return coating_tio2;
}


G4OpticalSurface* tio2::doOptTio2()
{
  coating_opSurf = new G4OpticalSurface(
      "TiO2Surface",
      glisur,
      ground,
      dielectric_metal,
      fExtrusionPolish
      );

  coating_properTable = new G4MaterialPropertiesTable();

  
  G4double p_TiO2[] = { 2.00*eV, 3.47*eV };
  const G4int nbins = sizeof(p_TiO2) / sizeof(G4double);

  G4double refl_TiO2[] = { fExtrusionReflectivity, fExtrusionReflectivity };
  assert(sizeof(refl_TiO2) == sizeof(p_TiO2));
 
  G4double effi_TiO2[] = {0, 0};
  assert(sizeof(effi_TiO2) == sizeof(p_TiO2));
  coating_properTable->AddProperty("REFLECTIVITY", p_TiO2, refl_TiO2, nbins);
  coating_properTable->AddProperty("EFFICIENCY", p_TiO2, effi_TiO2, nbins);
 
  coating_opSurf->SetMaterialPropertiesTable(coating_properTable);

  return coating_opSurf;
}
