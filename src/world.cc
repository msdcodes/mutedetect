// Geant4 Libraries
//
#include "G4PVPlacement.hh"
#include "G4LogicalVolume.hh"
#include "G4ThreeVector.hh"
#include "G4Material.hh"
#include "G4LogicalVolume.hh"
#include "G4Box.hh"
#include "G4UnitsTable.hh"
#include "G4SystemOfUnits.hh" 
#include "G4NistManager.hh"


// Local Libraries
//
#include "world.hh"


world::world()
{
  G4cout << "...world..." << G4endl;

  // Initializing for mechanical detector
  //
  world_geo = NULL;
  world_log = NULL;
  world_phys = NULL;
}


world::~world()
{}


void world::DefineMaterials()
{
  G4NistManager* nist = G4NistManager::Instance();

  world_mat
    = nist->FindOrBuildMaterial("G4_AIR");
}

void world::buildDetector(G4double dimenWorld, G4bool* overLaps)
{
  world_geo = 
    new G4Box("universe_geo",
        dimenWorld,
        dimenWorld,
        dimenWorld
        );
  
  world_log = 
    new G4LogicalVolume(world_geo,
        world_mat,
        world_geo->GetName(),
        0,
        0,
        0
        );

  world_phys =
    new G4PVPlacement(0,
        G4ThreeVector(0.*m, 0.*m, 0.*m),
        world_log,
        world_geo->GetName(),
        0,
        false,
        0,
        overLaps
        );
}


G4LogicalVolume* world::getLogVolume()
{
  return world_log;
}


G4VPhysicalVolume* world::getPhysVolume()
{
  return world_phys;
}
