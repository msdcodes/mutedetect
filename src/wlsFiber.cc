// Geant4 Libraries
//
#include "G4Material.hh"
#include "G4NistManager.hh"
#include "G4OpticalSurface.hh"
#include "G4LogicalBorderSurface.hh"
#include "G4PhysicalConstants.hh"


// Local Libraries
//
#include "wlsFiber.hh"
#include "pmmaFiber.hh"
#include "fiberClad1.hh"
#include "fiberClad2.hh"


// C++ Libraries
//
#include "string.h"


wlsFiber::wlsFiber()
{
  fWLSfiberRX = 0.5*mm;

  clad1 = new fiberClad1(fWLSfiberRX);
  clad2 = new fiberClad2();
}


wlsFiber::~wlsFiber()
{}


void wlsFiber::buildDetector(G4LogicalVolume* log_mother, std::string wlsName, std::string cld1Name, std::string cld2Name, int copy, G4double fHoleLength, G4Material* pmma, G4Material* cldMat, G4bool* overLaps)
{
  G4double fWLSfiberZ = fHoleLength - 2.5*mm;

  clad2->buildDetector(log_mother, cld2Name, copy, fWLSfiberZ, clad1->getRX(), fWLSfiberRX, cldMat, overLaps);

  clad1->buildDetector(log_mother, cld1Name, copy, fWLSfiberZ, fWLSfiberRX, cldMat, overLaps);


  // ======================
  // *** Building Fiber ***
  wlsFiber_geo = 
    new G4Tubs(
        wlsName,
        0.,
        fWLSfiberRX,
        fWLSfiberZ,
        0.0*rad,
        twopi*rad
        );

  wlsFiber_log =
    new G4LogicalVolume(wlsFiber_geo,
        pmma,
        wlsName,
        0,
        0,
        0
        );

  wlsFiber_phys =
    new G4PVPlacement(
        0,
        G4ThreeVector(),
        wlsFiber_log,
        wlsName,
        log_mother,
        false,
        copy,
        overLaps
        );
}


G4VPhysicalVolume* wlsFiber::getPhysVolume()
{
  return wlsFiber_phys;
}


G4LogicalVolume* wlsFiber::getLogVolume()
{
  return wlsFiber_log;
}
