#ifndef scintCont_h
#define scintCont_h 1


// Geant4 Libraries
//
#include "G4Material.hh"
#include "G4LogicalVolume.hh"
#include "G4PVPlacement.hh"
#include "G4Box.hh"
#include "G4Tubs.hh"
#include "G4SystemOfUnits.hh"


// Local Libraries
//


class scintCont
{
  public:
    scintCont();
    virtual ~scintCont();

    void buildDetector(G4LogicalVolume* log_mother, G4RotationMatrix* rot, G4ThreeVector pos, std::string nameClone, int copy, G4String barName, G4double fBarHigth, G4double fBarLength, G4double fBarWidth, G4Material* coating_tio2, G4OpticalSurface* coating_opSurf, G4bool* overLaps);

    G4VPhysicalVolume* getPhysVolume();
    G4LogicalVolume* getLogVolume();

  private:

    G4Box* coating_geo;
    G4LogicalVolume* coating_log;
    G4VPhysicalVolume* coating_phys;
};
#endif
