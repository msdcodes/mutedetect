#!/bin/bash

fPart="tmpMachin24h.shw.bz2"

nLines=37500
totLines=173183300

rm sp-*

for i in $(seq ${nLines} ${nLines} ${totLines});
do 
  bzcat ${fPart} | head -${i} | tail -${nLines} > sp-${i}.dat
  bzip2 -z sp-${i}.dat
done
