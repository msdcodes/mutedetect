#ifndef expHall_h
#define expHall_h 1


// Geant4 Libraries
//
#include "G4Material.hh"
#include "G4LogicalVolume.hh"
#include "G4PVPlacement.hh"
#include "G4Box.hh"
#include "G4SystemOfUnits.hh" 


// Local Libraries
//


class expHall
{
  public:
    expHall();
    virtual ~expHall();

    void DefineMaterials();
    void buildDetector(G4double expHall_size, G4LogicalVolume* log_mother, G4bool* overLaps);

    G4VPhysicalVolume* getPhysVolume();
    G4LogicalVolume* getLogVolume();

  private:

    G4Material* expHall_matter;
    G4Box* expHall_geo;
    G4LogicalVolume* expHall_log;
    G4VPhysicalVolume* expHall_phys;
};
#endif
