#ifndef TrackingAction_h
#define TrackingAction_h 1


// Geant4 Libraries
//
#include "G4UserTrackingAction.hh"
#include "G4Track.hh"
#include "G4UserEventAction.hh"


// C++ Libraries
//
#include <string>


class EventAction;


class TrackingAction : public G4UserTrackingAction
{
  public:
    TrackingAction(EventAction* eventAction);
    virtual ~TrackingAction();

    virtual void PreUserTrackingAction(const G4Track*);
//    virtual void PostUserTrackingAction(const G4Track*);

    EventAction* fEventAction;

    static G4ThreadLocal int primary;
    static G4ThreadLocal int optical;
    static G4ThreadLocal int secondary;


  protected:
    G4TrackingManager* fpTrackingManager;

  
  private:
    
};
#endif
