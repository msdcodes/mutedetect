#ifndef wlsFiber_h
#define wlsFiber_h 1


// Geant4 Libraires
//
#include "G4Material.hh"
#include "G4LogicalVolume.hh"
#include "G4PVPlacement.hh"
#include "G4Tubs.hh"
#include "G4SystemOfUnits.hh" 
#include "G4ReflectionFactory.hh"


// C++ Libraries
//
#include "string.h"


// Local Libraries
//
#include "fiberClad1.hh"
#include "fiberClad2.hh"


class wlsFiber
{
  public:
    wlsFiber();
    virtual ~wlsFiber();

    void buildDetector(G4LogicalVolume* log_mother, std::string wlsName, std::string cld1Name, std::string cld2Name, int copy, G4double fHoleLength, G4Material* pmma, G4Material* cldMat, G4bool* overLaps);
  
    G4VPhysicalVolume* getPhysVolume();
    G4LogicalVolume* getLogVolume();


  private:

    G4Tubs* wlsFiber_geo;
    G4LogicalVolume* wlsFiber_log;
    G4VPhysicalVolume* wlsFiber_phys;

    G4double fWLSfiberRX;

    fiberClad1* clad1;
    fiberClad2* clad2;
};
#endif
