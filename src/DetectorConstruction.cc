
// Geant4 Libraries
//
#include "G4Material.hh"
#include "G4Element.hh"

#include "G4LogicalBorderSurface.hh"
#include "G4LogicalSkinSurface.hh"
#include "G4OpticalSurface.hh"
#include "G4MultiFunctionalDetector.hh"
#include "G4VPrimitiveScorer.hh"
#include "G4PSEnergyDeposit.hh"
#include "G4VPhysicalVolume.hh"

#include "G4Box.hh"
#include "G4Sphere.hh"
#include "G4LogicalVolume.hh"
#include "G4ThreeVector.hh"
#include "G4PVPlacement.hh"
#include "G4SystemOfUnits.hh"
#include "G4NistManager.hh"
#include "G4UnitsTable.hh"
#include "G4PhysicalConstants.hh"
#include "G4NistManager.hh" 


// Local Libraries
//
//#include "PMTSD.hh"
#include "DetectorConstruction.hh"
#include "world.hh"
#include "expHall.hh"
#include "muteBox.hh"
#include "volcano.hh"
#include "grdFloor.hh"
#include "scintCont.hh"
#include "scintCalo.hh"
#include "wlsFiber.hh"
#include "siPM.hh"
#include "tio2.hh"
#include "airOpt.hh"
#include "polystyrene.hh"
#include "pTerphenylPopop.hh"
#include "pmmaFiber.hh"
#include "pethyleneF.hh"


// C++ Libraries
//
#include <string>
#include "math.h"


DetectorConstruction::DetectorConstruction()
 : G4VUserDetectorConstruction()
{
  G4cout << "...DetectorConstruction..." << G4endl;

  universe = new world();
  theExpHall = new expHall();

  grd = new grdFloor();
  volc = new volcano();
  mtBox = new muteBox();

  wcdCont = new wcdLagoCont();
  wcdCalo = new wcdLagoCalo();
  pmt_det = new wcdpmt();

  sctCont0Y = new scintCont();
  sctCont0X = new scintCont();
  sctCont1Y = new scintCont();
  sctCont1X = new scintCont();

  sctCalo0Y = new scintCalo();
  sctCalo0X = new scintCalo();
  sctCalo1Y = new scintCalo();
  sctCalo1X = new scintCalo();

  fiber0Y = new wlsFiber();
  fiber0X = new wlsFiber();
  fiber1Y = new wlsFiber();
  fiber1X = new wlsFiber();

  sipm0Y = new siPM();
  sipm0X = new siPM();
  sipm1Y = new siPM();
  sipm1X = new siPM();
}


DetectorConstruction::~DetectorConstruction()
{}

// *************************
// Doing Mechanical Detector
// ************************* 

G4VPhysicalVolume* DetectorConstruction::Construct()
{                        
  G4bool checkOverlaps = true;
  G4double worldSize = 5.*km; // 5.*km

  universe->DefineMaterials();
  universe->buildDetector(worldSize, &checkOverlaps);

  theExpHall->DefineMaterials();
  theExpHall->buildDetector( worldSize, universe->getLogVolume(), &checkOverlaps );

  grd->DefineMaterials();
  grd->buildDetector( worldSize, theExpHall->getLogVolume(), &checkOverlaps );


  // ========================
  // *** Building Volcano ***
  G4double volcPxSemiA = 750.*m; // x
  G4double volcPySemiA = 750.*m; // y
  G4double volcPzSemiA = 300.*m; // Z
  G4ThreeVector* volcPost = new G4ThreeVector(1.*km, 0.*m, 0.*m);

  volc->DefineMaterials();
  volc->buildDetector(theExpHall->getLogVolume(), volcPxSemiA, volcPySemiA, volcPzSemiA, volcPost, &checkOverlaps);


  // ==========================
  // *** Building Detectors ***

  // ===============
  // *** MuTeBox ***
  G4double mtBox_dimX = 3.25*m;
  G4double mtBox_dimY = 1.*m;
  G4double mtBox_dimZ = 1.*m;
  G4RotationMatrix* mtBoxRot = new G4RotationMatrix();
  mtBoxRot->rotateY(0.*deg);
  G4ThreeVector mtBoxPos = G4ThreeVector(0.*m, 0.*m, 1.*m);


  mtBox->DefineMaterials();
  mtBox->buildDetector( mtBox_dimX, mtBox_dimY, mtBox_dimZ, theExpHall->getLogVolume(), mtBoxRot, mtBoxPos, &checkOverlaps );
 
  // ===========
  // *** WCD ***
  G4double wcdHight = 60.*cm;

  G4double pxSemiAxis = 10.1*cm;
  G4double pySemiAxis = 10.1*cm;
  G4double pzSemiAxis = 6.5*cm;
  
  G4ThreeVector* wcdContPos = new G4ThreeVector(0.*m, 0.*m, 0.*m);

  wcdCont->DefineMaterials();
  wcdCont->buildDetector(mtBox->getLogVolume(), &checkOverlaps, wcdHight, wcdContPos);

  wcdCalo->DefineMaterials();
  wcdCalo->buildDetector(wcdCont->getLogVolume(), wcdCont->getPhysVolume(), &checkOverlaps, wcdHight);

  G4ThreeVector* pmt_pos = new G4ThreeVector(0.*m, 0.*m, wcdHight);
  pmt_det->DefineMaterials();
  pmt_det->buildDetector(wcdCalo->getLogVolume(), pxSemiAxis, pySemiAxis, pzSemiAxis, pmt_pos, &checkOverlaps);


  // =================
  // *** Hodoscope ***
  
  // ======================================
  // *** Cloning and building hodoscope ***
  
  int nbars = 30;
  G4double scintWidth = 2.5*mm; // x
  G4double scintLength = 600.*mm; // y
  G4double scintHeight = 20.*mm; // z
  G4double holeRadius = 1.*mm;
  G4ThreeVector scintPos = G4ThreeVector(0.*mm, 0.*mm, 0.*mm);

  std::string bnCont = "p0TiO2Y";
  std::string bnBar = "p0bY";
  std::string bnBarHole = "p0bHoY";
  std::string bnFib = "p0fibY";

  std::string contName;
  std::string barName;
  std::string barHoName;
  std::string fibName;

  G4String bnclad1 = "p0clad1Y";
  G4String bnclad2 = "p0clad2Y";
  G4String bnwls = "p0wlsY";

  std::string clad1Name;
  std::string clad2Name;
  std::string wlsName;

  std::string bnsipm = "sipm";
  std::string sipmName;
  G4ThreeVector sipmPos = G4ThreeVector(0.*mm, 0.*mm, 0.*mm);

  // ==========================
  // *** Creating Materials ***
  tio2* _tio2 = new tio2();
  G4Material* coatMat = _tio2->doTiO2();
  G4OpticalSurface* coatOpt = _tio2->doOptTio2();

  airOpt* airHole = new airOpt();
  G4Material* holeMat = airHole->doAirOpt();

  polystyrene* polys = new polystyrene();
  G4Material* sctMat = polys->doPolyste();
  sctMat->SetMaterialPropertiesTable(polys->doOpPolyste());
  sctMat->GetIonisation()->SetBirksConstant(0.126*mm/MeV);

  
  // Making with pTerphenyl+POPOP
  //
  /*pTerphenylPopop* dopant = new pTerphenylPopop();
  G4Material* sctMat = dopant->doPter();
  sctMat->SetMaterialPropertiesTable(dopant->doOpPter());  
*/

  pmmaFiber* pmma = new pmmaFiber();
  G4Material* fiberMat = pmma->doPmma();
  fiberMat->SetMaterialPropertiesTable(pmma->doOpPmma());

  pethyleneF* pethy = new pethyleneF();
  G4Material* cladMat = pethy->doPethylene();
  cladMat->SetMaterialPropertiesTable(pethy->doOpPethylene());
 
  G4NistManager* nist = G4NistManager::Instance();
  G4Material* sipmMat = nist->FindOrBuildMaterial("G4_AIR");
 

  // ===================
  // *** For panel 0 ***
  G4double sctCont_posZ = -(mtBox_dimZ/2. + 80.*mm);
  G4RotationMatrix* sctContRotY = new G4RotationMatrix();
  sctContRotY->rotateX(90.*deg);

  sipmPos = G4ThreeVector(0.*mm, 0.*mm, 598.0*mm);
  nbars = 30;

  for ( int coat = 0; coat < nbars; coat++ )
  {
    contName = bnCont + std::to_string(coat);
    barName = bnBar + std::to_string(coat);
    barHoName = bnBarHole + std::to_string(coat);
    scintPos = G4ThreeVector(1000.*mm, 0.*mm, sctCont_posZ);

    sctCont0Y->buildDetector(mtBox->getLogVolume(), sctContRotY, scintPos, contName, coat, contName, scintHeight, scintLength, scintWidth, coatMat, coatOpt, &checkOverlaps);

    sctCalo0Y->buildDetector(sctCont0Y->getLogVolume(), barName, coat, barHoName, scintHeight, scintLength, scintWidth, holeRadius, scintLength, sctMat, holeMat, &checkOverlaps);

    // ====================
    // *** Making Fiber ***
    clad1Name = bnclad1 + std::to_string(coat);
    clad2Name = bnclad2 + std::to_string(coat);
    wlsName = bnwls + std::to_string(coat);

    fiber0Y->buildDetector(sctCalo0Y->getHoleLogVolume(), wlsName, clad1Name, clad2Name, coat, scintLength, fiberMat, cladMat, &checkOverlaps);

    sipmName = bnsipm + std::to_string(coat);
    sipm0Y->buildDetector(sctCalo0Y->getHoleLogVolume(), sipmPos, sipmName, coat, sipmMat, &checkOverlaps); 

    sctCont_posZ += 40.*mm;
  }

  bnCont = "p0TiO2K";
  bnBar = "p0bK";
  bnBarHole = "p0bHoK";

  bnclad1 = "p0clad1K";
  bnclad2 = "p0clad2K";
  bnwls = "p0wlsK";
  bnsipm = "sipm";

  G4double sctCont_posY = -580.*mm;
  G4RotationMatrix* sctContRotX = new G4RotationMatrix();

  for ( int coat = 0; coat < nbars; coat++ )
  {
    contName = bnCont + std::to_string(coat+30);
    barName = bnBar + std::to_string(coat+30);
    barHoName = bnBarHole + std::to_string(coat+30);
    scintPos = G4ThreeVector(1005.0*mm, sctCont_posY, scintLength + 0.5*mm - (mtBox_dimZ/2. + 100.*mm));

    sctCont0X->buildDetector(mtBox->getLogVolume(), sctContRotX, scintPos, contName, coat, contName, scintHeight, scintLength, scintWidth, coatMat, coatOpt, &checkOverlaps);

    sctCalo0X->buildDetector(sctCont0X->getLogVolume(), barName, coat, barHoName, scintHeight, scintLength, scintWidth, holeRadius, scintLength, sctMat, holeMat, &checkOverlaps);


    // ====================
    // *** Making Fiber ***
    clad1Name = bnclad1 + std::to_string(coat+30);
    clad2Name = bnclad2 + std::to_string(coat+30);
    wlsName = bnwls + std::to_string(coat+30);

    fiber0X->buildDetector(sctCalo0X->getHoleLogVolume(), wlsName, clad1Name, clad2Name, coat, scintLength, fiberMat, cladMat, &checkOverlaps);

    sipmName = bnsipm + std::to_string(coat+30);

    sipm0X->buildDetector(sctCalo0X->getHoleLogVolume(), sipmPos, sipmName, coat, sipmMat, &checkOverlaps); 

    sctCont_posY += 40.*mm;
  }


  // ===================
  // *** For panel 1 ***

  bnCont = "p1TiO2Y";
  bnBar = "p1bY";
  bnFib = "p1fibY";
  bnBarHole = "p1bHoY";

  bnclad1 = "p1clad1Y";
  bnclad2 = "p1clad2Y";
  bnwls = "p1wlsY";
  bnsipm = "sipm";

  sctCont_posZ = -(mtBox_dimZ/2. + 80.*mm);


  for ( int coat = 0; coat < nbars; coat++ )
  {
    contName = bnCont + std::to_string(coat+60);
    barName = bnBar + std::to_string(coat+60);
    barHoName = bnBarHole + std::to_string(coat+60);
    scintPos = G4ThreeVector(3000.*mm, 0.*mm, sctCont_posZ);

    sctCont1Y->buildDetector(mtBox->getLogVolume(), sctContRotY, scintPos, contName, coat, contName, scintHeight, scintLength, scintWidth, coatMat, coatOpt, &checkOverlaps);

    sctCalo1Y->buildDetector(sctCont1Y->getLogVolume(), barName, coat, barHoName, scintHeight, scintLength, scintWidth, holeRadius, scintLength, sctMat, holeMat, &checkOverlaps);


    // ====================
    // *** Making Fiber ***
    clad1Name = bnclad1 + std::to_string(coat+60);
    clad2Name = bnclad2 + std::to_string(coat+60);
    wlsName = bnwls + std::to_string(coat+60);

    fiber1Y->buildDetector(sctCalo1Y->getHoleLogVolume(), wlsName, clad1Name, clad2Name, coat, scintLength, fiberMat, cladMat, &checkOverlaps);

    sipmName = bnsipm + std::to_string(coat+60);

    sipm1Y->buildDetector(sctCalo1Y->getHoleLogVolume(), sipmPos, sipmName, coat, sipmMat, &checkOverlaps); 

    sctCont_posZ += 40.*mm;
  }


  bnCont = "p1TiO2K";
  bnBar = "p1bK";
  bnBarHole = "p1bHoK";

  bnclad1 = "p1clad1K";
  bnclad2 = "p1clad2K";
  bnwls = "p1wlsK";
  bnsipm = "sipm";
  
  sctCont_posY = -580.*mm;

  for ( int coat = 0; coat < nbars; coat++ )
  {
    contName = bnCont + std::to_string(coat+90);
    barName = bnBar + std::to_string(coat+90);
    barHoName = bnBarHole + std::to_string(coat+90);
    scintPos = G4ThreeVector(3005.0*mm, sctCont_posY, scintLength + 0.5*mm - (mtBox_dimZ/2. + 100.*mm));

    sctCont1X->buildDetector(mtBox->getLogVolume(), sctContRotX, scintPos, contName, coat, contName, scintHeight, scintLength, scintWidth, coatMat, coatOpt, &checkOverlaps);

    sctCalo1X->buildDetector(sctCont1X->getLogVolume(), barName, coat, barHoName, scintHeight, scintLength, scintWidth, holeRadius, scintLength, sctMat, holeMat, &checkOverlaps);


    // ====================
    // *** Making Fiber ***
    clad1Name = bnclad1 + std::to_string(coat+90);
    clad2Name = bnclad2 + std::to_string(coat+90);
    wlsName = bnwls + std::to_string(coat+90);

    fiber1X->buildDetector(sctCalo1X->getHoleLogVolume(), wlsName, clad1Name, clad2Name, coat, scintLength, fiberMat, cladMat, &checkOverlaps);

    sipmName = bnsipm + std::to_string(coat+90);

    sipm1X->buildDetector(sctCalo1X->getHoleLogVolume(), sipmPos, sipmName, coat, sipmMat, &checkOverlaps); 

    sctCont_posY += 40.*mm;
  }

  return universe->getPhysVolume();
}
