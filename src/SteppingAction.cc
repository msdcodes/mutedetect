
// Geant4 Libraries
//
#include "G4Step.hh"
#include "G4Track.hh"

#include "G4OpticalPhoton.hh"
#include "G4VProcess.hh"
#include "G4ParticleDefinition.hh"
#include "G4ParticleTypes.hh"
#include "G4SystemOfUnits.hh"

#include "G4Event.hh"
#include "G4RunManager.hh"
#include "g4root.hh"
#include "G4SDManager.hh"
#include "G4TransportationManager.hh"
#include "G4SystemOfUnits.hh"


// Local Libraries
#include "SteppingAction.hh"
#include "DetectorConstruction.hh"
#include "EventAction.hh"
#include "TrackingAction.hh"
#include "PrimaryGeneratorAction.hh"
#include "pmtHit.hh"
#include "sipmHit.hh"


// C++ Libraries
//
#include "string.h"



G4ThreadLocal std::string SteppingAction::volumName;
G4ThreadLocal std::string SteppingAction::askSipm;

G4ThreadLocal int SteppingAction::trackTime;
G4ThreadLocal int SteppingAction::sipmN;
G4ThreadLocal double SteppingAction::mtTime;

G4ThreadLocal pmtHit* SteppingAction::pmtH;
G4ThreadLocal sipmHit* SteppingAction::sipmH;

G4ThreadLocal G4Track* SteppingAction::track;



SteppingAction::SteppingAction(EventAction* eventAction, TrackingAction* trackAction)
  : G4UserSteppingAction(),
    fEventAction(eventAction),
    ftrackAction(trackAction)
{
  G4cout << "...SteppingAction..." << G4endl; 
 
  G4TransportationManager::GetTransportationManager()
    ->GetNavigatorForTracking()->SetPushVerbosity(0);
  
  
  detectorConstruction 
     = static_cast < const DetectorConstruction* 
     > (G4RunManager::GetRunManager()
         ->GetUserDetectorConstruction());
         

  pmtH = new pmtHit();
  sipmH = new sipmHit();
}

SteppingAction::~SteppingAction()
{}


void SteppingAction::UserSteppingAction(const G4Step* step)
{
  // ===========================
  // *** Doing for Primaries ***

  if ( ftrackAction->primary )
  {
    volumName = step->GetPreStepPoint()->GetPhysicalVolume()->GetName();

    track = step->GetTrack();
  
    if ( volumName == "universe_geo" || volumName == "Ground_geo" )
      track->SetTrackStatus(fStopAndKill);
  
    if ( volumName == "muteBox_geo" && fEventAction->prmTime )
    {
      mtTime = track->GetGlobalTime() / (1.*ns);
      ftrackAction->primary = 0;
      fEventAction->prmTime = 0;
    }

    if ( volumName == "Volcano_geo" )
    {
      fEventAction->trcRock += step->GetStepLength();
      if ( step->GetPostStepPoint()->GetPhysicalVolume()->GetName() == "expHall_geo" )
      {
        (*fEventAction->prmOutVolcPoi) = step->GetPostStepPoint()->GetPosition();
        (*fEventAction->prmOutVolcMom) = step->GetPostStepPoint()->GetMomentum();
      }
    }
  }


  // ======================================
  // *** Killing secondaries in Volcano ***

  
  if ( ftrackAction->secondary && step->GetPreStepPoint()->GetPhysicalVolume()->GetName() == "Volcano_geo" )
    step->GetTrack()->SetTrackStatus(fKillTrackAndSecondaries);


  // =================================
  // *** Doing for optical photons ***
  if ( ftrackAction->optical )
  {
    volumName = step->GetPostStepPoint()->GetPhysicalVolume()->GetName();
    track = step->GetTrack();

    // ----------------------------------
    // *** Asking if sipm was reached ***
    askSipm = volumName.substr(0, 4);

    if ( askSipm == "sipm" )
    {
      if ( sipmH->askDetected( track->GetTotalEnergy() ) )
      {
        sipmN = std::stoi( volumName.substr(4, 4) );
        trackTime 
          = ( track->GetGlobalTime() / (1.*ns) - mtTime );

        if ( trackTime < 300 ) // TraceLenght
          (*fEventAction->trcSipm)[trackTime][sipmN]++;

        /* 
        if ( (*fEventAction->trcSipm)[2][sipmN] == 3 ) // Threshold of 3 photoelectrons on bin 3
        {
          fEventAction->sipmThr = 1;
          if ( (*fEventAction->detecTimeSipm)[sipmN] > 0 
              && (track->GetGlobalTime() / (1.*ns)) < (*fEventAction->detecTimeSipm)[sipmN] )
          {
            (*fEventAction->detecTimeSipm)[sipmN] = track->GetGlobalTime() / (1.*ns);

          G4cout << "MSD"
            << " " << (*fEventAction->detecTimeSipm)[sipmN]
            << " " << sipmN
            << " " << track->GetGlobalTime() / (1.*ns) 
            << G4endl;
          }
        }
        */
      }
      track->SetTrackStatus(fStopAndKill);
    }


    // ---------------------------------
    // *** Asking if PMT was reached ***   
    if ( volumName == "pmt_geo" )
    {
      if ( pmtH->askDetected( track->GetTotalEnergy() ) )
      {
        trackTime 
          = ( track->GetGlobalTime() / (1.*ns) - mtTime )/ 10;

        if ( trackTime < 30 ) // TraceLenght
          (*fEventAction->trcPmt)[trackTime]++;

        if ( (*fEventAction->trcPmt)[2]++ > 3 ) // Threshold of 3 photoelectrons on bin 3
          fEventAction->pmtThr = 1;
      }
      track->SetTrackStatus(fStopAndKill);
    }
  }
}
