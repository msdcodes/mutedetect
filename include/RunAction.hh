#ifndef RunAction_h
#define RunAction_h 1


// Geant4 Libraries
//
#include "globals.hh"
#include "G4UserRunAction.hh"
#include "g4root.hh"


// Local Libraries
//
#include "PrimarySpectrum.hh"


// C++ Libraries
//
#include <fstream>
#include <iostream>
#include <string>


class G4Run;


class RunAction : public G4UserRunAction
{
  public:
    RunAction();
    virtual ~RunAction();

    virtual void BeginOfRunAction(const G4Run* aRun);
    virtual void EndOfRunAction(const G4Run* aRun);

    std::ofstream* outFile;


  private:
};
#endif 
