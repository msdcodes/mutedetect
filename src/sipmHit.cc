
// Geant4 Libraries
// 
#include "G4SystemOfUnits.hh"
#include "Randomize.hh"


// Local Libraries
// 
#include "sipmHit.hh"


G4ThreadLocal G4Allocator<sipmHit>* sipmHitAllocator=0;


sipmHit::sipmHit()
  : waveLength(0)
{}


sipmHit::~sipmHit()
{}

G4int sipmHit::askDetected(G4double energy)
{

  waveLength = 1240. / (energy/(1.*eV));
  G4double rand = G4UniformRand();
  G4int isdetected = 0;

  
  if (waveLength >= 300. && waveLength < 350.)
    rand < 0.07 ? isdetected = 1 : isdetected = 0;
  
  else if (waveLength >= 350. && waveLength < 400.)
    rand < 0.25 ? isdetected = 1 : isdetected = 0;
  
  else if (waveLength >= 400. && waveLength < 450.)
    rand < 0.37 ? isdetected = 1 : isdetected = 0;
  
  else if (waveLength >= 450. && waveLength < 500.)
    rand < 0.40 ? isdetected = 1 : isdetected = 0;
  
  else if (waveLength >= 500. && waveLength < 550.)
    rand < 0.35 ? isdetected = 1 : isdetected = 0;

  else if (waveLength >= 550. && waveLength < 600.)
    rand < 0.29 ? isdetected = 1 : isdetected = 0;

  else if (waveLength >= 600. && waveLength < 650.)
    rand < 0.23 ? isdetected = 1 : isdetected = 0;

  else if (waveLength >= 650. && waveLength < 700.)
    rand < 0.18 ? isdetected = 1 : isdetected = 0;

  else if (waveLength >= 700. && waveLength < 750.)
    rand < 0.13 ? isdetected = 1 : isdetected = 0.;

  else if (waveLength >= 750. && waveLength < 800.)
    rand < 0.10 ? isdetected = 1 : isdetected = 0;

  else if (waveLength >= 800. && waveLength < 850.)
    rand < 0.07 ? isdetected = 1 : isdetected = 0;

  else if (waveLength >= 850. && waveLength < 900.)
    rand < 0.04 ? isdetected = 1 : isdetected = 0;
  
  else
   isdetected = 0;

  return isdetected;
}

