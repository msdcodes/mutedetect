#ifndef tio2_h
#define tio2_h 1


// Geant4 Libraries
//
#include "G4Material.hh"
#include "G4Element.hh"
#include "G4OpticalSurface.hh"
#include "G4MaterialPropertiesTable.hh"


// Local Libraries
//


class tio2
{
  public:
    tio2();
    ~tio2();
    G4Material* doTiO2();
    G4OpticalSurface* doOptTio2();

  private:

    G4Material* coating_tio2;
    G4Material* TiO2;
    G4Material* Polystyrene;

    G4OpticalSurface* coating_opSurf;

    G4MaterialPropertiesTable* coating_properTable;

    G4Element* C;
    G4Element* O;
    G4Element* H;
    G4Element* Ti;

    G4double fExtrusionReflectivity = 0.;
    G4double fExtrusionPolish = 0.;

};
#endif

