// Geant4 Libraries
//
#include "G4VUserDetectorConstruction.hh"
#include "G4PVPlacement.hh"
#include "G4LogicalVolume.hh"
#include "G4ThreeVector.hh"
#include "G4Material.hh"
#include "G4Element.hh"
#include "G4LogicalVolume.hh"
#include "G4UnitsTable.hh"
#include "G4SystemOfUnits.hh" 
#include "G4OpticalSurface.hh"
#include "G4LogicalBorderSurface.hh"
#include "G4NistManager.hh"


// Local Libraries
//
#include "expHall.hh"


expHall::expHall()
{}


expHall::~expHall()
{}


void expHall::DefineMaterials()
{
  G4NistManager* nist = G4NistManager::Instance();
  expHall_matter = nist->FindOrBuildMaterial("G4_AIR");
}


void expHall::buildDetector(G4double expHall_size, G4LogicalVolume* log_mother, G4bool* overLaps)
{
  expHall_size = expHall_size * .5;

  expHall_geo =
    new G4Box("expHall_geo",
        expHall_size,
        expHall_size,
        expHall_size
        );

  expHall_log =
    new G4LogicalVolume(expHall_geo,
      expHall_matter,
      expHall_geo->GetName(),
      0,
      0,
      0
      );

  expHall_phys =
    new G4PVPlacement(0,
      G4ThreeVector(0.*m, 0.*m, 0.*m),
      expHall_log,
      expHall_log->GetName(),
      log_mother,
      false,
      0,
      overLaps
      );
}


G4VPhysicalVolume* expHall::getPhysVolume()
{
  return expHall_phys;
}


G4LogicalVolume* expHall::getLogVolume()
{
  return expHall_log;
}
