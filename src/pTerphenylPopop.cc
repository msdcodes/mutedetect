// Geant4 Libraries
//
#include "G4Material.hh"
#include "G4Element.hh"
#include "G4SystemOfUnits.hh"
#include "G4NistManager.hh"


// C++ Libraries
//


// Local Libraries
//
#include "pTerphenylPopop.hh"


pTerphenylPopop::pTerphenylPopop()
{
  G4cout << "...Creating pTerphenyl+POPOP..." << G4endl;

  pTer = NULL;
  popop = NULL;
  ppo = NULL;
  pTerphenyl = NULL;

  z = 0.;
  a = 0.;
  density = 0.;
}


pTerphenylPopop::~pTerphenylPopop()
{}


G4Material* pTerphenylPopop::doPter()
{
  G4NistManager* nist = G4NistManager::Instance();


  // Making basic elements
  //
  C = new G4Element("Carbon", "C", z = 6. , a = 12.01*g/mole);
  O = new G4Element("Oxigen", "O", z = 8., a = 15.99*g/mole);
  H = new G4Element("Hydrogen", "H", z = 1., a = 1.01*g/mole);
  N = new G4Element("Nitrogen", "N", z = 7., a = 14.01*g/mole);


  // Making PPO
  //
  ppo = new G4Material("ppo", density = 0.722*g/cm3, 4);
  ppo->AddElement(C, 15);
  ppo->AddElement(H, 11);
  ppo->AddElement(N, 1);
  ppo->AddElement(O, 1);


  // Making POPOP
  popop = new G4Material("popop", density = 1.204*g/cm3, 4);
  popop->AddElement(C, 24);
  popop->AddElement(O, 2);
  popop->AddElement(H, 16);
  popop->AddElement(N, 2);


  // Making p-Terphenyl
  //
  pTerphenyl = new G4Material("pTerphenyl", density = 1.23*g/cm3, 2);
  pTerphenyl->AddElement(C, 18);
  pTerphenyl->AddElement(H, 14);

  
  //Define Scintillator material
  //
  pTer = new G4Material("pTer", density = 1.06*g/cm3, 3);

  pTer->AddMaterial(nist->FindOrBuildMaterial("G4_POLYSTYRENE"), 98.97*perCent); // 98%
  pTer->AddMaterial(popop, 0.03*perCent); // 0.3%
  pTer->AddMaterial(ppo, 1.*perCent);
  //pTer->AddMaterial(pTerphenyl, 2.0*perCent); // 2%

  return pTer;
}


G4MaterialPropertiesTable* pTerphenylPopop::doOpPter()
{
  // Add optical properties to Scint
  // Number of bins for Scintillator material properties table
  const G4int nEntries = 5;

  G4double PhotonEnergy[nEntries] = { 2.02*eV, 2.8*eV, 3*eV, 3.2*eV, 3.75*eV};
  G4double RefractiveIndexScint[nEntries] = { 1.5, 1.5,  1.5, 1.5, 1.5 };
  G4double AbsorptionScint[nEntries] = {2.*cm,  2.*cm,  2.*cm, 2.*cm, 2.*cm};
  G4double ScintilFast[nEntries] = { 0.0, 0.125, 0.5, 0.25, 0.125};


  // Define property table for Scint
  G4MaterialPropertiesTable* myMPTScint = new G4MaterialPropertiesTable();
 
  myMPTScint->AddProperty("RINDEX",PhotonEnergy, RefractiveIndexScint, nEntries);
  myMPTScint->AddProperty("ABSLENGTH",PhotonEnergy, AbsorptionScint, nEntries);
  myMPTScint->AddProperty("FASTCOMPONENT",PhotonEnergy, ScintilFast, nEntries);
  myMPTScint->AddConstProperty("SCINTILLATIONYIELD",50/MeV);
  myMPTScint->AddConstProperty("RESOLUTIONSCALE",1.0);
  myMPTScint->AddConstProperty("FASTTIMECONSTANT", 3.*ns);

  myMPTScint->AddConstProperty("YIELDRATIO", 0.8);
 
  return myMPTScint;
}
