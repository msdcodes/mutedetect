#ifndef SteppingAction_h
#define SteppingAction_h 1


// Geant4 Libraries
//
#include "G4UserSteppingAction.hh"
#include "globals.hh"


// Local Libraries
//
#include "DetectorConstruction.hh"
#include "pmtHit.hh"
#include "sipmHit.hh"


// C++ Libraries
//
#include "string.h"


class EventAction;
class TrackingAction;
class PrimaryGeneratorAction;


/// Stepping action class
/// 

class SteppingAction : public G4UserSteppingAction
{
  public:
    SteppingAction(EventAction* eventAction, TrackingAction* trackAction);
    virtual ~SteppingAction();

    // method from the base class
    virtual void UserSteppingAction(const G4Step*);
    EventAction* fEventAction;
    TrackingAction* ftrackAction;

  private:

    const DetectorConstruction *detectorConstruction;

    static G4ThreadLocal std::string volumName;
    static G4ThreadLocal std::string askSipm;

    static G4ThreadLocal int trackTime;
    static G4ThreadLocal int sipmN;
    static G4ThreadLocal double mtTime;

    static G4ThreadLocal pmtHit* pmtH;
    static G4ThreadLocal sipmHit* sipmH;

    static G4ThreadLocal G4Track* track;

};

#endif
