
// Geant4 Libraries
//
#include "G4Timer.hh"
#include "G4Run.hh"
#include "g4root.hh"
#include "G4AccumulableManager.hh"


// Root Libraries
//
#include <TFile.h> 


// Local Libraries
//
#include "RunAction.hh"


// C++ Libraries
//
#include <fstream>


RunAction::RunAction()
 : G4UserRunAction()
{
  G4cout << "...RunAction..." << G4endl;  
}


RunAction::~RunAction()
{}


void RunAction::BeginOfRunAction(const G4Run* aRun)
{
  G4cout << "### Run " << aRun->GetRunID() << " start." << G4endl;
  outFile = new std::ofstream();
  outFile->open("muteSimuData.dat"); 
}


void RunAction::EndOfRunAction(const G4Run* aRun)
{
  G4cout << "number of event = " << aRun->GetNumberOfEvent() << G4endl;
  outFile->close();
}
