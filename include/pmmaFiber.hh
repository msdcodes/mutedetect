#ifndef pmmaFiber_h
#define pmmaFiber_h 1


// Geant4 Libraries
//
#include "G4Material.hh"
#include "G4Element.hh"


// C++ Libraries
//
#include "string.h"


// Local Libraries
//


class pmmaFiber
{
  public:
    pmmaFiber();
    ~pmmaFiber();

    G4Material* doPmma();
    G4MaterialPropertiesTable* doOpPmma();


  private:
    G4Element* C;
    G4Element* H;
    G4Element* O;

    G4Material* thePmma;
    G4double z;
    G4double a;
    G4double density;
};
#endif
