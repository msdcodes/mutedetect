// Geant4 Libraries
//
#include "G4VUserDetectorConstruction.hh"
#include "G4PVPlacement.hh"
#include "G4LogicalVolume.hh"
#include "G4ThreeVector.hh"
#include "G4Material.hh"
#include "G4Element.hh"
#include "G4LogicalVolume.hh"
#include "G4UnitsTable.hh"
#include "G4SystemOfUnits.hh" 
#include "G4OpticalSurface.hh"
#include "G4LogicalBorderSurface.hh"
#include "G4NistManager.hh"


// Local Libraries
//
#include "muteBox.hh"


muteBox::muteBox()
{}


muteBox::~muteBox()
{}


void muteBox::DefineMaterials()
{
  G4NistManager* nist = G4NistManager::Instance();
  muteBox_matter = nist->FindOrBuildMaterial("G4_AIR");
}


void muteBox::buildDetector(G4double muteBox_x, G4double muteBox_y, G4double muteBox_z, G4LogicalVolume* log_mother, G4RotationMatrix* rot, G4ThreeVector pos, G4bool* overLaps)
{
  muteBox_geo =
    new G4Box("muteBox_geo",
        muteBox_x,
        muteBox_y,
        muteBox_z
        );

  muteBox_log =
    new G4LogicalVolume(muteBox_geo,
      muteBox_matter,
      muteBox_geo->GetName(),
      0,
      0,
      0
      );

  muteBox_phys =
    new G4PVPlacement(
        rot,
        pos,
        muteBox_log,
        muteBox_log->GetName(),
        log_mother,
        false,
        0,
        overLaps
        );
}


G4VPhysicalVolume* muteBox::getPhysVolume()
{
  return muteBox_phys;
}


G4LogicalVolume* muteBox::getLogVolume()
{
  return muteBox_log;
}
