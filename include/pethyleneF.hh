#ifndef pethyleneF_h
#define pethyleneF_h 1


//Geant4 Libraries
//
#include "G4Material.hh"
#include "G4Element.hh"


// Local Libraries
//


class pethyleneF
{
  public:
    pethyleneF();
    ~pethyleneF();

    G4Material* doPethylene();
    G4Material* doFpethylene();

    G4MaterialPropertiesTable* doOpPethylene();
    G4MaterialPropertiesTable* doOpFpethylene();


  private:
    G4Element* C;
    G4Element* H;

    G4Material* thePethylene;
    G4Material* theFpethylene;

    G4double z;
    G4double a;
    G4double density;
};
#endif

