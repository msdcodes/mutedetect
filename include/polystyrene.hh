#ifndef polystyrene_h
#define polystyrene_h 1


// Geant4 Libraries
//
#include "G4Material.hh"
#include "G4Element.hh"


// Local Libraries
//


class polystyrene
{
  public:
    polystyrene();
    ~polystyrene();

    G4Material* doPolyste();
    G4MaterialPropertiesTable* doOpPolyste();


  private:
    G4Element* C;
    G4Element* H;

    G4Material* thePolyste;
    G4double z;
    G4double a;
    G4double density;
};
#endif

