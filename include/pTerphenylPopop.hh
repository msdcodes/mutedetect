#ifndef pTerphenylPopop_h
#define pTerphenylPopop_h 1


// Geant4 Libraries
//
#include "G4Material.hh"
#include "G4Element.hh"


// Local Libraries
//


class pTerphenylPopop
{
  public:
    pTerphenylPopop();
    ~pTerphenylPopop();

    G4Material* doPter();
    G4MaterialPropertiesTable* doOpPter();


  private:

    G4Material* pTer;
    G4Material* popop;
    G4Material* ppo;
    G4Material* pTerphenyl;

    double z;
    double a;
    double density;

    G4Element* C;
    G4Element* O;
    G4Element* H;
    G4Element* N;

};
#endif

