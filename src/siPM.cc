// Geant4 Libraries
//
#include "G4PVPlacement.hh"
#include "G4LogicalVolume.hh"
#include "G4ThreeVector.hh"
#include "G4Material.hh"
#include "G4LogicalVolume.hh"
#include "G4UnitsTable.hh"
#include "G4SystemOfUnits.hh" 
#include "G4NistManager.hh"
#include "G4Box.hh"


// Local Libraries
//
#include "siPM.hh"


siPM::siPM()
{
  G4cout << "...SiPM..." << G4endl;

  // Initializing for mechanical detector
  //
  sipm_geo = NULL;
  sipm_log = NULL;
  sipm_phy = NULL;
}


siPM::~siPM()
{}


void siPM::buildDetector(G4LogicalVolume* log_mother, G4ThreeVector pos, std::string name, int copy, G4Material* sipm_mat, G4bool* overLaps) 
{
  sipm_geo
    = new G4Box(
        name,
        0.65*mm, // x
        0.65*mm, // y
        0.5*mm // z
        );

  sipm_log
    = new G4LogicalVolume(
        sipm_geo,
        sipm_mat,
        name
        );

  sipm_phy
    =  new G4PVPlacement(
        0,
        pos, 
        sipm_log,
        name,
        log_mother,
        false,
        copy,
        overLaps
        );
}


G4VPhysicalVolume* siPM::getPhysVolume()
{
  return sipm_phy;
}
