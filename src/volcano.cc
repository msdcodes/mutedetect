// Database of materials:
// http://geant4-userdoc.web.cern.ch/geant4-userdoc/UsersGuides/ForApplicationDeveloper/BackupVersions/V9.3/html/apas09.html


// Geant4 Libraries
//
#include "G4Element.hh"
#include "G4Material.hh"
#include "G4NistManager.hh"



// Local Libraries
//
#include "volcano.hh"


volcano::volcano()
{
  G4cout << "...Creating Volcano..." << G4endl;
}


volcano::~volcano()
{}


void volcano::DefineMaterials()
{
  G4double a = 0.;
  G4double z;
  G4String symbol;
  G4int natoms;

  G4double density = 2.65*g/cm3;

	// Creating Si
	//
	a = 28.09*g/mole;
  G4Element* elSi 
		= new G4Element(
				"Silicon", 
				symbol="Si", 
				z=14.,
        a
				);

  // Creating Oxigen
	//
	a = 16.00*g/mole;
	G4Element* elO  
		= new G4Element(
        "Oxygen",
				symbol="O",
        z= 8.,
        a
        );

	// Creating Standard Rock
	//
	volcano_mat = new G4Material(
      "Rock",
      density,
      2
      );

	volcano_mat->AddElement(elSi, natoms=1);
	volcano_mat->AddElement(elO,  natoms=2);
}


void volcano::buildDetector(G4LogicalVolume* log_mother, G4double pxSemiAxis, G4double pySemiAxis, G4double pzSemiAxis, G4ThreeVector* volcPost, G4bool* overLaps)
{

  G4double pzBottomCut = 0.*m; //-pzSemiAxis;
  G4double pzTopCut = pzSemiAxis;//0.*cm;

  volcano_geo
    = new G4Ellipsoid(
        "Volcano_geo",
        pxSemiAxis,
        pySemiAxis,
        pzSemiAxis,
        pzBottomCut,
        pzTopCut
        );

  volcano_log
    = new G4LogicalVolume(
        volcano_geo,
        volcano_mat,
        volcano_geo->GetName()
        );

  volcano_phys 
    = new G4PVPlacement(
        0,
        G4ThreeVector(volcPost->x(), volcPost->y(), volcPost->z()),
        volcano_log,
        volcano_geo->GetName(),
        log_mother,
        false,
        0,
        overLaps
        );
}


G4VPhysicalVolume* volcano::getPhysVolume()
{
  return volcano_phys;
}


G4LogicalVolume* volcano::getLogVolume()
{
  return volcano_log;
}
